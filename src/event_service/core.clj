(ns event-service.core
  (:use [org.httpkit.server :only [run-server]])
  (:require [compojure.core :refer :all]
            [compojure.handler :refer [site]]
            [compojure.route :as route]
            [ring.middleware.reload :as reload]
            [event-service.api :as api]))

(defn in-dev? [_] true) ;; TODO read a config variable from command line, env, or file?

(defn -main [& args] ;; entry point, lein run will pick up and start from here
  (let [handler (if (in-dev? args)
                  (reload/wrap-reload (site #'api/all-routes)) ;; only reload when dev
                  (site api/all-routes))]
    (run-server handler {:port 8080})))
