(ns event-service.api
  (:require [compojure.core :refer :all]
            [compojure.route :as route]
            [liberator.core :refer [defresource]]
            [ring.middleware.reload :as reload]
            [event-service.service :as service]))

(defresource event [page-id]
  :allowed-methods [:get]
  :available-media-types ["application/json"]
  :handle-ok (fn [_] (service/fetch-events page-id)))

(defroutes all-routes
  (GET "/" [] "handling-page")
  (context "/api/v1/event" []
           (ANY "/:page-id" [page-id] (event page-id)))
  (route/not-found "<p>Page not found.</p>")) ;; all other, return 404
