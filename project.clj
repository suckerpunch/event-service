(defproject event-service "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [org.clojure/tools.cli "0.2.2"]
                 [org.clojure/tools.nrepl "0.2.10"]
                 [org.clojure/tools.logging "0.2.6"]
                 [org.slf4j/slf4j-log4j12 "1.7.12"]
                 [cider/cider-nrepl "0.9.1"]
                 [compojure "1.3.4"]
                 [cheshire "5.0.1"]
                 [clj-http "2.0.0"]
                 [clj-time "0.9.0"]
                 [congomongo "0.4.1"]
                 [liberator "0.13"]
                 [http-kit "2.1.18"]
                 [prismatic/schema "0.3.0"]
                 [ring/ring-core "1.3.2"]
                 [ring/ring-defaults "0.1.2"]
                 [ring/ring-devel "1.3.2"]
                 [ring/ring-json "0.3.1"]
                 [ring-server "0.4.0"]]
  :main event-service.core
  :plugins [[lein-ring "0.8.13"]]
  :profiles
  {:dev {:dependencies [[javax.servlet/servlet-api "2.5"]
                        [ring-mock "0.1.5"]]}})
